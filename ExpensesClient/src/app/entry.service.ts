import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  baseUrl: string = 'http://localhost:59659/api/entries/'

  constructor(private http: HttpClient) { }

  getEntry(id) {
    return this.http.get(this.baseUrl + '/' + id);
  }

  getAll(token) {    
    var headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    const httpOptions = {
      headers: headers_object,
      withCredentials: true
    };

    console.log('Object username: ', httpOptions);

    return this.http.get(this.baseUrl);
  }

  createEntry(entry) {
    return this.http.post(this.baseUrl + 'add/', entry);
  }

  updateEntry(id, entry) {
    return this.http.put(this.baseUrl + '/' + id, entry);
  }

  deleteEntry(id) {
    return this.http.delete(this.baseUrl + id);
  }
}
