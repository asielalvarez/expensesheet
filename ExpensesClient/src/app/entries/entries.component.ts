import { Component, OnInit, ViewChild } from '@angular/core';
import { EntryService } from '../entry.service';
import { DataSource } from '@angular/cdk/table';
import { MatTableDataSource, MatDialog, MatSort, MatPaginator } from '@angular/material';
import { EntryElement } from 'interfaces/EntryElement';
import { UpdateEntryComponent } from '../update-entry/update-entry.component';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.css']
})
export class EntriesComponent implements OnInit {

  displayedColumns: string[] = ['Description', 'IsExpense', 'Value', 'Actions']
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: EntryService,
    private dialog: MatDialog,
    private authService: AuthService) { }

  ngOnInit() {
    if(this.authService.isAuthenticated) {
      var token = localStorage.getItem('token_value');
      console.log('token: ', token);
        this.service.getAll(token).subscribe((data:any) => {
        console.log('Result - ', data);
        this.dataSource = new MatTableDataSource<EntryElement>(data as EntryElement[]);
        this.dataSource.paginator = this.paginator;
      })
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  updateEntry(entry) {
    console.log(entry);
    this.dialog.open(UpdateEntryComponent, {
      data: {
        Id: entry.Id,
        Description: entry.Description,
        IsExpense: entry.IsExpense,
        Value: entry.Value
      }
    })
  }

}
