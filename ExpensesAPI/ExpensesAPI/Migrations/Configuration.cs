namespace ExpensesAPI.Migrations
{
    using ExpensesAPI.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ExpensesAPI.Data.AppDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ExpensesAPI.Data.AppDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            //context.Entries.Add(new Entry() { Description = "test", IsExpense = false, Value = 10.11 });

            User u = new User();
            u.Id = 0;
            u.Username = "a";
            u.Password = "1";

            Entry e = new Entry();
            e.Id = 0;
            e.Description = "toy";
            e.IsExpense = true;
            e.Value = 13.2;

            User_Entry ue = new User_Entry();
            ue.Id = 0;
            ue.User_Id = u.Id;
            ue.Entry_Id = e.Id;

            context.Users.Add(u);
            context.Entries.Add(e);
            context.Users_Entries.Add(ue);
        }
    }
}
