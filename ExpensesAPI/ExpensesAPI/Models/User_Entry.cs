﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExpensesAPI.Models
{
    public class User_Entry
    {
        [Key]
        public int Id { get; set; }

        // Foreing keys
        public int User_Id { get; set; }
       // public User User { get; set; }
        public int Entry_Id { get; set; }
        // public Entry Entry { get; set; }
    }
}

/*  
 *  MIGRATIONS   
 * Add-Migration <name>
 * Update-Database -v
 */