﻿using ExpensesAPI.Data;
using ExpensesAPI.Filters;
using ExpensesAPI.Models;
using ExpensesAPI.Token_Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ExpensesAPI.Controllers
{    
    [EnableCors("http://localhost:4200", "*", "*")]
    [AuthFilter]
    [Authorize]
    public class EntriesController : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetEntry(int id)
        {

            var token = this.Request.Headers.Authorization.Parameter;

            string username = TokenValidation.getUsername(token);

            if (username == null)
            {
                return NotFound();
            }

            try
            {
                using (var context = new AppDbContext())
                {
                    var entry =
                        context.Users_Entries
                        .Where(x => x.User_Id == context.Users.FirstOrDefault(n => n.Username == username).Id)
                        .Join(
                            context.Entries,
                            userEntry => userEntry.Entry_Id,
                            e => e.Id,
                            (userEntry, e) => new
                            {
                                Id = e.Id,
                                Description = e.Description,
                                IsExpense = e.IsExpense,
                                Value = e.Value
                            })
                        .Where(x => x.Id == id).FirstOrDefault();

                    if(entry == null)
                    {
                        return NotFound();
                    }

                    return Ok(entry);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Route("api/entries/")]
        [HttpGet]
        public IHttpActionResult GetEntries()
        {

            var token = this.Request.Headers.Authorization.Parameter;

            string username = TokenValidation.getUsername(token);

            if(username == null)
            {
                return NotFound();
            }

            try
            {
                using (var context = new AppDbContext())
                {
                    var entries =
                       context.Users_Entries
                       .Where(x => x.User_Id == context.Users.FirstOrDefault(n => n.Username == username).Id)
                       .Join(
                           context.Entries,
                           userEntry => userEntry.Entry_Id,
                           e => e.Id,
                           (userEntry, e) => new
                           {
                               Id = e.Id,
                               Description = e.Description,
                               IsExpense = e.IsExpense,
                               Value = e.Value
                           }).ToList();

                    return Ok(entries);
                }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPost]
        public IHttpActionResult PostEntry([FromBody]Entry entry)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var token = this.Request.Headers.Authorization.Parameter;

            string username = TokenValidation.getUsername(token);

            if (username == null)
            {
                return NotFound();
            }

            try
            {
                using (var context = new AppDbContext())
                {
                    // Add entry to Entry Table
                    var e = context.Entries.Add(entry);
                    context.SaveChanges();

                    User u = context.Users.FirstOrDefault(n => n.Username == username);
                    User_Entry ue = new User_Entry();
                    ue.User_Id = u.Id;
                    ue.Entry_Id = e.Id;

                    // Add relation to User_Entry Table
                    context.Users_Entries.Add(ue);
                    context.SaveChanges();

                    return Ok("Entry was Created!");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPut]
        public IHttpActionResult UpdateEntry(int id, [FromBody]Entry entry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var token = this.Request.Headers.Authorization.Parameter;

            string username = TokenValidation.getUsername(token);

            if (username == null)
            {
                return NotFound();
            }

            if (id != entry.Id)
            {
                return BadRequest();
            }

            try
            {
                using (var context = new AppDbContext())
                {                    
                    var checkUser = context.Users_Entries
                       .Where(x => x.User_Id == context.Users.FirstOrDefault(n => n.Username == username).Id && x.Entry_Id == id).FirstOrDefault();

                    if(checkUser == null)
                    {
                        return NotFound();
                    }

                    var entryToUpdate = context.Entries.FirstOrDefault(x => x.Id == id);

                    if (entry == null)
                    {
                        return NotFound();
                    }

                    entryToUpdate.Description = entry.Description;
                    entryToUpdate.IsExpense = entry.IsExpense;
                    entryToUpdate.Value = entry.Value;

                    context.SaveChanges();

                    return Ok("Entry updated!");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteEntry(int id)
        {
            var token = this.Request.Headers.Authorization.Parameter;

            string username = TokenValidation.getUsername(token);

            if (username == null)
            {
                return NotFound();
            }

            try
            {
                using (var context = new AppDbContext())
                {
                    var checkUser = context.Users_Entries
                      .Where(x => x.User_Id == context.Users.FirstOrDefault(n => n.Username == username).Id && x.Entry_Id == id).FirstOrDefault();

                    if (checkUser == null)
                    {
                        return NotFound();
                    } 

                    var entry = context.Entries.FirstOrDefault(n => n.Id == id);

                    if (entry == null)
                    {
                        return NotFound();
                    }

                    context.Users_Entries.Remove(checkUser);
                    context.Entries.Remove(entry);
                    context.SaveChanges();

                    return Ok("Entry deleted");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }       
        
    }
}
