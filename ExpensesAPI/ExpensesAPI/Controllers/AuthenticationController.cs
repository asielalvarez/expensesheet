﻿using ExpensesAPI.Data;
using ExpensesAPI.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ExpensesAPI.Controllers
{
    [EnableCors("http://localhost:4200", "*", "*")]
    [RoutePrefix("auth")]
    public class AuthenticationController : ApiController
    {
        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Login([FromBody] User user)
        {
            if(string.IsNullOrEmpty(user.Username) || string.IsNullOrEmpty(user.Password))
            {
                return BadRequest("Enter your username and password");
            }

            try
            {
                using(var context = new AppDbContext())
                {
                    var users = context.Users.ToList().FindAll(n => n.Username == user.Username && n.Password == user.Password);

                    bool exists = false;

                    foreach (var u in users)
                    {
                        if(u.Username.Equals(user.Username) && u.Password.Equals(user.Password))
                        {
                            exists = true;
                            break;
                        }
                    }    

                    if(exists) { 
                        return Ok(CreateToken(user));
                    }
                    else
                    {
                        return BadRequest("Wrong credentials");
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Register([FromBody] User user)
        {
            try
            {
                using (var context  = new AppDbContext())
                {
                    var users = context.Users.ToList().FindAll(n => n.Username == user.Username);

                    bool exists = false;

                    foreach (var u in users)
                    {
                        if (u.Username.Equals(user.Username))
                        {
                            exists = true;
                            break;
                        }
                    }

                    if (exists)
                    {
                        return BadRequest("User already exists");
                    }

                    context.Users.Add(user);
                    context.SaveChanges();

                    return Ok(CreateToken(user));
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }        

        private JwtPackage CreateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.Name, user.Username)
            });

            const string secretKey = "your secret key goes here";
            var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(secretKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var token = (JwtSecurityToken)tokenHandler.CreateJwtSecurityToken(
                subject: claims,
                signingCredentials: signingCredentials
            );

            var tokenString = tokenHandler.WriteToken(token);

            return new JwtPackage()
            {
                Username = user.Username,
                Token = tokenString
            };
        }
    }
}

public class JwtPackage
{
    public string Token { get; set; }
    public string Username { get; set; }
}