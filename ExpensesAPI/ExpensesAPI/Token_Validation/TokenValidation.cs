﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;

namespace ExpensesAPI.Token_Validation
{
    public class TokenValidation
    {               
        public static IPrincipal getPrincipal(string token)
        {
            return ValidateToken(token, out SecurityToken x);
        }

        public static string getUsername(string token)
        {
            var username = string.Empty;

            SecurityToken validatedToken;
            IPrincipal principal = ValidateToken(token, out validatedToken);
            
            if(principal != null && validatedToken != null)
            {
                var t = validatedToken as JwtSecurityToken;
                username = t.Claims.ToList().ElementAt(0).Value;
            }

            return username;
        }

        private static IPrincipal ValidateToken(string authToken, out SecurityToken validatedToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = GetValidationParameters();

            IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
            //var t = validatedToken as JwtSecurityToken;

            //var temp = principal.Identity.Claims.FindFirst(ClaimTypes.Name);

            //username = t.Claims.ToList().ElementAt(0).Value;

            return principal;
        }

        private static TokenValidationParameters GetValidationParameters()
        {
            const string key = "your secret key goes here";
            return new TokenValidationParameters()
            {
                ValidateLifetime = false, // Because there is no expiration in the generated token
                ValidateAudience = false, // Because there is no audiance in the generated token
                ValidateIssuer = false,   // Because there is no issuer in the generated token
                ValidIssuer = "Sample",
                ValidAudience = "Sample",
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)) // The same key as the one that generate the token
            };
        }

    }
}