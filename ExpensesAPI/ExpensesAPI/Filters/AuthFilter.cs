﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ExpensesAPI.Token_Validation;

namespace ExpensesAPI.Filters
{
    public class AuthFilter : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);

            HttpContext.Current.User = TokenValidation.getPrincipal(actionContext.Request.Headers.Authorization.Parameter);
            
        }
    }
}