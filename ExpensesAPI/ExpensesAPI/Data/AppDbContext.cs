﻿using ExpensesAPI.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ExpensesAPI.Data
{
    public class AppDbContext : DbContext
    {

        /*protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }*/

        public AppDbContext() : base("name=ExpensesDb")
        {

        }

        public DbSet<Entry> Entries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<User_Entry> Users_Entries { get; set; }
    }
}